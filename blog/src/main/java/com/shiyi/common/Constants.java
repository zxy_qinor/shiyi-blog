package com.shiyi.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  <p> 全局常用变量 </p>
 *
 * @description :
 * @author : by blue
 * @date : 2019/10/12 14:47
 */
public class Constants {

    /**
     * 用户状态
     */
    public static final int USER_STATUS_ONE = 1;

    public static final int USER_STATUS_ZERO = 0;

    /**
     * 未知的
     */
    public static final String UNKNOWN = "未知";

    /**
     * 省
     */
    public static final String PROVINCE = "省";

    /**
     * 市
     */
    public static final String CITY = "市";


    public static final String PRE_TAG = "<span style=\"color:red\">";

    public static final String POST_TAG = "</span>";

    public static final int USER_ROLE_ID = 2;

    public static final String CURRENT_USER = "current_user";

    public static final String PAGE_NO = "pageNo";

    public static final String PAGE_SIZE = "pageSize";

    public static final String DEFAULT_SIZE = "10";

}
