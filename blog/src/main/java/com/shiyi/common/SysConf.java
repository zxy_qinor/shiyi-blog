package com.shiyi.common;

public class SysConf{

    public static final Long OTHER_CATEGORY_ID = 16L;

    public static final Long ADMIN_USER_ID = 7L;

    public static final String ONE = "1";

    public static final String ZERO = "0";

    public static final String DEFAULT_VALUE = "defaultValue";

    public static final String LIST = "list";

    public static final String LIMIT_ONE = "LIMIT 1";

    public static final int ROLE_ID = 2;

    public static final String RECORDS = "records";

    public static final String CURRENTPAGE = "currentPage";

}
