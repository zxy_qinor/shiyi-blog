package com.shiyi.dto;

import lombok.Data;

@Data
public class ContributeDTO {
    private Integer count;
    private String date;
}
